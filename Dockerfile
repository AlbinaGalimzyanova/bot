FROM maven:3.5

COPY . src/

WORKDIR src/

RUN mvn clean install assembly:single

CMD java -jar target/Project-1.0-SNAPSHOT-jar-with-dependencies.jar
