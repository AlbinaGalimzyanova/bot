package models.service;

import models.entity.Question;

import java.sql.SQLException;

public class QuestionsService extends Service<Question> {
    public QuestionsService() throws SQLException {
        super("quest");
    }
}