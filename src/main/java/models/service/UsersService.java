package models.service;

import models.entity.User;

import java.sql.SQLException;

public class UsersService extends Service<User> {
    public UsersService() throws SQLException {
        super("db/users");
    }
}