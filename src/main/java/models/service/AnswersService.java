package models.service;

import models.entity.Answer;

import java.sql.SQLException;

public class AnswersService extends Service<Answer> {
    public AnswersService() throws SQLException {
        super("quest");
    }
}