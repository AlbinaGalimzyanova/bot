package models.service;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

import java.lang.reflect.ParameterizedType;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public abstract class Service<T> {
    private ConnectionSource source;
    private Dao<T, String> dao;

    public Dao<T, String> getDao() {
        return dao;
    }

    public Service(String databaseName) throws SQLException {
        source = new JdbcConnectionSource("jdbc:sqlite:" + databaseName + ".db");

        dao = DaoManager.createDao(source, (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0]);
    }

    public List<T> getAll() throws SQLException {
        return dao.queryForAll();
    }

    public int create(T t) throws SQLException {
        return dao.create(t);
    }

    public void createIfNotExists(T t) throws SQLException {
        dao.createIfNotExists(t);
    }

    public int update(T t) throws SQLException {
        return dao.update(t);
    }

    public List<T> find(Map<String, Object> where) throws SQLException {
        return dao.queryForFieldValues(where);
    }

    public void delete(T t) throws SQLException {
        dao.delete(t);
    }
}