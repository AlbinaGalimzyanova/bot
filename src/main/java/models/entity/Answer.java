package models.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable (tableName = "answers")
public class Answer {
    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    private int question_id;
    @DatabaseField
    private String text;
    @DatabaseField
    private int next_question_id;

    public Answer() {

    }

    public void setId(int id) {
        this.id = id;
    }
    public void setQuestion_id(int question_id) {
        this.question_id = question_id;
    }
    public void setText(String text) {
        this.text = text;
    }
    public void setNext_question_id(int next_question_id) {
        this.next_question_id = next_question_id;
    }

    public int getId() {
        return id;
    }
    public int getQuestion_id() {
        return question_id;
    }
    public String getText() {
        return text;
    }
    public int getNext_question_id() {
        return next_question_id;
    }

    @Override
    public String toString() {
        return "Answer {" +
                ", id = " + id +
                ", question_id = " + question_id +
                ", text = " + text +
                ", next_question_id = " + next_question_id +
                "}";
    }
}