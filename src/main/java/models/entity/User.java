package models.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "users")
public class User {
    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    private Long telegram_id;
    @DatabaseField
    private int question_id;

    public User() {

    }

    public void setId(int id) {
        this.id = id;
    }
    public void setTelegram_id(Long telegram_id) {
        this.telegram_id = telegram_id;
    }
    public void setQuestion_id(int question_id) {
        this.question_id = question_id;
    }

    public int getId() {
        return id;
    }
    public Long getTelegram_id() {
        return telegram_id;
    }
    public int getQuestion_id() {
        return question_id;
    }

    @Override
    public String toString() {
        return "Question {" +
                "id = " + id +
                ", telegram_id = " + telegram_id +
                ", question_id = " + question_id +
                '}';
    }
}
