package models.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "questions")
public class Question {
    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    private String text;

    public Question() {

    }

    public void setId(int id) {
        this.id = id;
    }
    public void setText(String text) {
        this.text = text;
    }

    public int getId() {
        return id;
    }
    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "Question {" +
                "id = " + id +
                ", text = \"" + text + "\"" +
                "}";
    }
}