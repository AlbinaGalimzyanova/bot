package telegram.commands;

import models.entity.User;
import models.service.UsersService;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static telegram.bot.Sender.sendQuestion;

public class Start implements ICommand {
    public SendMessage run(Message message) {
        User user = new User();

        Map<String, Object> map = new HashMap<>();
        map.put("telegram_id", message.getChatId());

        try {
            UsersService usersService = new UsersService();
            List<User> users = usersService.find(map);

            if (users.size() == 0) {
                user.setQuestion_id(1);
                user.setTelegram_id(message.getChatId());

                usersService.create(user);
            } else {
                user = users.get(0);
                user.setQuestion_id(1);
                System.out.println(user);
                usersService.update(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return sendQuestion(user, message);
    }
}