package telegram.commands;

import models.entity.Answer;
import models.entity.User;
import models.service.AnswersService;
import models.service.UsersService;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import telegram.bot.Sender;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static telegram.bot.Sender.sendQuestion;

public class Default implements ICommand {
    @Override
    public SendMessage run(Message message) {
        try {
            UsersService usersService = new UsersService();
            Map<String, Object> map = new HashMap<>();
            map.put("telegram_id", message.getChatId());
            List<User> users = usersService.find(map);
            User user = users.get(0);

            Map<String, Object> map1 = new HashMap<>();
            map1.put("question_id", user.getQuestion_id());
            map1.put("text", message.getText());

            AnswersService answersService = new AnswersService();
            List<Answer> answers = answersService.find(map1);
            if (answers.size() > 0) {
                Answer answer = answers.get(0);
                user.setQuestion_id(answer.getNext_question_id());
                usersService.update(user);

                return sendQuestion(user, message);
            } else {
                user.setQuestion_id(0);
                usersService.update(user);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return (new SendMessage()).setChatId(message.getChatId()).setText(Sender.getEnd());
    }
}