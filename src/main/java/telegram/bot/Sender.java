package telegram.bot;

import models.entity.Answer;
import models.entity.Question;
import models.entity.User;
import models.service.AnswersService;
import models.service.QuestionsService;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Sender {
    private static String end = "Квест завершен, для перезапуска выберите команду /start";

    public static String getEnd() {
        return end;
    }

    public static SendMessage sendQuestion(User user, Message message) {
        try {
            QuestionsService questionsService = new QuestionsService();
            Map<String, Object> map = new HashMap<>();

            if (user.getQuestion_id() == 0) {
                return (new SendMessage())
                        .setChatId(message.getChatId())
                        .setText(end);
            }

            map.put("id", user.getQuestion_id());

            List<Question> questions = questionsService.find(map);

            if (questions.size() == 0) {
                throw new Exception("Question not found");
            }

            Question question = questions.get(0);

            Map<String, Object> map1 = new HashMap<>();
            map1.put("question_id", question.getId());

            AnswersService answersService = new AnswersService();
            final List<Answer> answers = answersService.find(map1);

            ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
            replyKeyboardMarkup.setSelective(true);
            replyKeyboardMarkup.setResizeKeyboard(true);
            replyKeyboardMarkup.setOneTimeKeyboad(true);

            List<KeyboardRow> keyboard = new ArrayList<>();

            KeyboardRow row = new KeyboardRow();

            answers.forEach(element -> row.add(element.getText()));

            keyboard.add(row);
            replyKeyboardMarkup.setKeyboard(keyboard);

            SendMessage sendMessage = new SendMessage();
            sendMessage.enableMarkdown(true);

            sendMessage.setReplyToMessageId(message.getMessageId());
            sendMessage.setChatId(message.getChatId());
            sendMessage.setReplyMarkup(replyKeyboardMarkup);
            sendMessage.setText(question.getText());

            return sendMessage;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}