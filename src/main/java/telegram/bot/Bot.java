package telegram.bot;

import database.CreateAnswers;
import database.CreateQuestions;
import database.CreateUsers;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import telegram.commands.Default;
import telegram.commands.Help;
import telegram.commands.Start;

import java.io.File;

public class Bot extends TelegramLongPollingBot {
    private static String botToken = "361623449:AAHpixNgJoxCzjqgDzDYY8pckF8JwU37pfQ";
    private static String testBotToken = "388478849:AAFoyqfRCTuzHF41DUfblaUbZMPWUyvGDt8";
    private String currentBotToken = "";

    private static String botUserName = "textquesttext_bot";
    private static String testBotUserName = "";
    private String currentBotUserName = "";

    public static void main(String[] args) {
        ApiContextInitializer.init();
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();

        boolean isTest = false;
        if (args.length > 0 && args[0].equals("--test")) {
            isTest = true;
        }

        File quest = new File("quest.db");
        File users = new File("db/users.db");

        if (!quest.exists()) {
            CreateQuestions.main();
            CreateAnswers.main();
        }

        if (!users.exists()) {
            CreateUsers.main();
        }

        try {
            telegramBotsApi.registerBot(new Bot(isTest));
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public Bot(boolean isTest) {
        this.currentBotToken = isTest ? testBotToken : botToken;
        this.currentBotUserName = isTest ? testBotUserName : botUserName;
    }

    public String getBotUsername() {
        return currentBotUserName;
    }
    public String getBotToken() {
        return currentBotToken;
    }

    public void onUpdateReceived(Update update) {
        Message message = update.getMessage();
        if (message != null && message.hasText()) {
            String currentMessage = message.getText();

            try {
                switch (currentMessage) {
                    case "/start":
                        Start start = new Start();
                        sendMessage(start.run(message));
                        break;

                    case "/help":
                        Help help = new Help();
                        sendMessage(help.run(message));
                        break;

                    default:
                        Default d = new Default();
                        sendMessage(d.run(message));
                        break;
                }
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendMessage(Message message, String text) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(message.getChatId().toString());
        sendMessage.setReplyToMessageId(message.getMessageId());
        sendMessage.setText(text);

        try {
            sendMessage(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}