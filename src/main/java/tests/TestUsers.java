package tests;

import models.entity.User;
import models.service.UsersService;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;
import java.util.*;

import static org.junit.Assert.assertTrue;

public class TestUsers {
    private static User user = new User();
    private static UsersService service;

    private static List<User> expectedList = new ArrayList<>();
    private static List<User> realityList = new ArrayList<>();
    private static List<User> expectedUpdated = new ArrayList<>();
    private static List<User> realityUpdated = new ArrayList<>();

    @BeforeClass
    public static void test() throws SQLException {
        user.setQuestion_id(1);
        user.setTelegram_id((long) 999999999);
        expectedList.add(user);

        service = new UsersService();
        service.getDao().executeRaw("BEGIN;");
        service.create(user);
        Map<String, Object> map = new HashMap<>();
        map.put("id", user.getId());
        realityList = service.find(map);

        service.update(user);
        expectedUpdated.add(user);
        Map<String, Object> map1 = new HashMap<>();
        map1.put("id", user.getId());
        realityUpdated = service.find(map1);
    }

    @Test
    public void createTest() {
        assertTrue(Objects.equals(expectedList.get(0).getTelegram_id(), realityList.get(0).getTelegram_id()));
        assertTrue(Objects.equals(expectedList.get(0).getQuestion_id(), realityList.get(0).getQuestion_id()));
    }

    @Test
    public void updateTest() {
        assertTrue(Objects.equals(expectedUpdated.get(0).getTelegram_id(), realityUpdated.get(0).getTelegram_id()));
        assertTrue(Objects.equals(expectedUpdated.get(0).getQuestion_id(), realityUpdated.get(0).getQuestion_id()));
    }

    @AfterClass
    public static void end() throws SQLException {
        service.getDao().executeRaw("ROLLBACK;");
    }
}