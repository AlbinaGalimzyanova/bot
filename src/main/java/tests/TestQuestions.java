package tests;

import models.entity.Question;
import models.service.QuestionsService;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;
import java.util.*;

import static org.junit.Assert.assertTrue;

public class TestQuestions {
    private static Question question = new Question();
    private static QuestionsService service;

    private static List<Question> expected = new ArrayList<>();
    private static List<Question> reality = new ArrayList<>();

    @BeforeClass
    public static void test() throws SQLException {
        question.setText("Вопрос 1");
        expected.add(question);

        service = new QuestionsService();
        service.getDao().executeRaw("BEGIN;");
        service.create(question);
        Map<String, Object> map = new HashMap<>();
        map.put("id", question.getId());
        reality = service.find(map);
    }

    @Test
    public void readTest() {
        assertTrue(Objects.equals(expected.get(0).getText(), reality.get(0).getText()));
    }

    @AfterClass
    public static void end() throws SQLException {
        service.getDao().executeRaw("ROLLBACK;");
    }
}