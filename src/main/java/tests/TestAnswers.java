package tests;

import models.entity.Answer;
import models.service.AnswersService;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;
import java.util.*;

import static org.junit.Assert.assertTrue;

public class TestAnswers {
    private static Answer answer = new Answer();
    private static AnswersService service;

    private static List<Answer> expected = new ArrayList<>();
    private static List<Answer> reality = new ArrayList<>();

    @BeforeClass
    public static void test() throws SQLException {
        answer.setQuestion_id(1);
        answer.setText("Вариант ответа 2");
        answer.setNext_question_id(3);
        expected.add(answer);

        service = new AnswersService();
        service.getDao().executeRaw("BEGIN;");
        service.create(answer);
        Map<String, Object> map = new HashMap<>();
        map.put("id", answer.getId());
        reality = service.find(map);
    }

    @Test
    public void answerTest() {
        assertTrue(Objects.equals(expected.get(0).getQuestion_id(), reality.get(0).getQuestion_id()));
        assertTrue(Objects.equals(expected.get(0).getText(), reality.get(0).getText()));
        assertTrue(Objects.equals(expected.get(0).getNext_question_id(), reality.get(0).getNext_question_id()));
    }

    @AfterClass
    public static void end() throws SQLException {
        service.getDao().executeRaw("ROLLBACK;");
    }
}