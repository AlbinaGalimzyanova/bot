package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class CreateAnswers {
    public static void main() {
        Connection connection;
        Statement statement;

        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:quest.db");

            System.out.println("Database opened successfully");

            statement = connection.createStatement();

            String sql = "CREATE TABLE answers " +
                    "(id              INTEGER PRIMARY KEY     AUTOINCREMENT," +
                    "question_id                      INTEGER NOT NULL," +
                    "text                             TEXT    NOT NULL," +
                    "next_question_id                 INTEGER)";

            statement.executeUpdate(sql);

            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }

        System.out.println("Table created successfully");
    }
}