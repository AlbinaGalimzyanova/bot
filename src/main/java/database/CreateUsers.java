package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class CreateUsers {
    public static void main() {
        Connection connection;
        Statement statement;

        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:db/users.db");

            System.out.println("Database opened successfully");

            statement = connection.createStatement();

            String sql1 = "CREATE TABLE users " +
                    "(id         INTEGER PRIMARY KEY     AUTOINCREMENT," +
                    "telegram_id INTEGER         KEY," +
                    "question_id                 INTEGER NOT NULL)";

            String sql2 = "CREATE UNIQUE INDEX IF NOT EXISTS users_telegram_id_index ON users (telegram_id)";

            statement.executeUpdate(sql1);
            statement.executeUpdate(sql2);

            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }

        System.out.println("Table created successfully");
    }
}